package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {
	
	//Dependency Injection for ProductRepository
	@Autowired
	ProductRepository productRepo;
	public Product updateProduct(Product product) {
		Product prod = productRepo.save(product);
		return prod;
	}

	
}
