package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
public class ProductController {
	
	//Dependency Injection for ProductDao
	@Autowired
	ProductDao productDao;

	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		Product prod = productDao.updateProduct(product);
		return prod;
	}
	
}